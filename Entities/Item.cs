using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
    ///<summary>
    /// Classe de interação com a tabela Item.
    ///</summary>
    public class Item
    {
        ///<summary>
        /// Id do Item, chave primária auto-incrementada, tipo int
        ///</summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int ItemId       { get; set; }
        ///<summary>
        /// Descrição do Item, tipo string
        ///</summary>
        public string Descrição { get; set; }
        ///<summary>
        /// Quantidade do Item, tipo int
        ///</summary>
        public int Quantidade   { get; set; }
        ///<summary>
        /// Id da venda relacionada, tipo int
        ///</summary>
        public int VendaId      { get; set; }
    }
}