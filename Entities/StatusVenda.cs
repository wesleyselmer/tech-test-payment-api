using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
    ///<summary>
    /// Lista de possíveis status da venda
    ///</summary>
    public enum EnumStatusVenda
    {
        ///<summary>
        /// Status 0 - Aguardando Pagamento
        ///</summary>
        AguardandoPagamento,
        ///<summary>
        /// Status 1 - Pagamento aprovado
        ///</summary>
        PagamentoAprovado,
        ///<summary>
        /// Status 2 - Enviado para Transportadora
        ///</summary>
        EnviadoParaTransportadora,
        ///<summary>
        /// Status 3 - Entregue
        ///</summary>
        Entregue,
        ///<summary>
        /// Status 4 - Cancelada
        ///</summary>
        Cancelada
    }
}