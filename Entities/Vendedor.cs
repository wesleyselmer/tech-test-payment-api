using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace tech_test_payment_api.Entities
{
    ///<summary>
    /// Classe de interação com a tabela Vendedor
    ///</summary>    
    [Index(nameof(CPF), IsUnique = true)]
    public class Vendedor
    {
        ///<summary>
        /// Id do Vendedor, chave primária e auto-incrementada, tipo int
        ///</summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int VendedorId   { get; set; }
        ///<summary>
        /// CPF do Vendedor, tipo string
        ///</summary>
        public string CPF       { get; set; }
        ///<summary>
        /// Nome do Vendedor, tipo string
        ///</summary>
        public string Nome      { get; set; }
        ///<summary>
        /// Email do vendedor, tipo string
        ///</summary>
        public string Email     { get; set; }
        ///<summary>
        /// Telefone do vendedor, tipo string
        ///</summary>
        public string Telefone  { get; set; }
    }
}