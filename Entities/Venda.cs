using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
    ///<summary>
    /// Classe de interação com a tabela Venda
    ///</summary>
    public class Venda
    {
        ///<summary>
        /// Id da Venda, chave primária e auto-incrementado, tipo int
        ///</summary>
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int VendaId              { get; set; }
        ///<summary>
        /// Id do Vendedor, tipo int
        ///</summary>
        public int VendedorId           { get; set; }
        ///<summary>
        /// Informações do vendedor, tipo vendedor
        ///</summary>
        public Vendedor Vendedor        { get; set; }
        ///<summary>
        /// Status da Venda, tipo EnumStatusVenda
        ///</summary>
        public EnumStatusVenda Status   { get; set; }
        ///<summary>
        /// Data da Venda, tipo DateTime
        ///</summary>
        public DateTime Data            { get; set; }
        ///<summary>
        /// Itens da venda, tipo lista de Item
        ///</summary>
        public List<Item> Items         { get; set; }
    }
}