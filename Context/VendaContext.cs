using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Context
{
    ///<summary>
    /// Cria a relação entre o banco de dados e as Entities do projeto
    ///</summary>

    public class VendaContext : DbContext
    {
        ///<summary>
        ///Método usado para definir a data da venda de forma automática
        ///</summary>

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Venda>()
            .Property(b => b.Data)
            .HasDefaultValueSql("getdate()");
        }

        ///<summary>
        /// Definição de quais tabelas precisam ser relacionadas
        ///</summary>
        
        public VendaContext(DbContextOptions<VendaContext> options) : base(options)
        {

        }

        ///<summary>
        /// Tabela responsável por arquivar os itens das vendas
        ///</summary>
        public DbSet<Item>      Itens       { get; set; }
        ///<summary>
        ///Tabela responsável por arquivar as vendas
        ///</summary>
        public DbSet<Venda>     Vendas      { get; set; }
        ///<summary>
        ///Tabela responsável por arquivar os vendedores
        ///</summary>
        public DbSet<Vendedor>  Vendedores  { get; set; }
    }
}