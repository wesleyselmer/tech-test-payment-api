using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Controllers
{
    ///<summary>
    /// Classe responsável pela interação com a tabela Vendedores
    ///</summary>
    [ApiController]
    [Route("controller")]
    public class VendedorController : ControllerBase
    {
        private readonly VendaContext _context;

        ///<summary>
        /// Definição da variável de interação context
        ///</summary>
        public VendedorController( VendaContext context )
        {
            _context = context;
        }
        ///<summary>
        /// Método para obter todos os vendedores.
        ///</summary>
        ///<returns>Retorna uma lista com todos os vendedores</returns>
        [HttpGet("ObterTodosVendedores")]
        public IActionResult ObterTodosVendedores()
        {
            var vendedores = _context.Vendedores.ToList();

            return Ok(vendedores);
        }
        ///<summary>
        /// Método para obter um vendedor por Id.
        ///</summary>
        ///<param name="id"></param>
        ///<returns>Retorna o vendedor solicitado, caso exista.</returns>
        [HttpGet("{id}")]
        public IActionResult ObterVendedorPorId(int id)
        {
            var vendedor = _context.Vendedores.Find(id);

            if(vendedor == null)
                return NotFound();

            return Ok(vendedor);
        }
        ///<summary>
        /// Método para obter um vendedor por Nome.
        ///</summary>
        ///<param name="nome"></param>
        ///<returns>Retorna o vendedor solicitado, caso exista.</returns>
        [HttpGet("ObterPorNome")]
        public IActionResult ObterVendedorPorNome(string nome)
        {
            var vendedor = _context.Vendedores.Where(x => x.Nome.Contains(nome));

            if (vendedor == null)
                return NotFound();

            return Ok(vendedor);
        }
        ///<summary>
        /// Método para obter um vendedor por CPF.
        ///</summary>
        ///<param name="cpf"></param>
        ///<returns>Retorna o vendedor solicitado, caso exista.</returns>    
        [HttpGet("ObterPorCPF")]
        public IActionResult ObterVendedorPorCPF(string cpf)
        {
            var vendedor = _context.Vendedores.Where(x => x.CPF == cpf);

            if (vendedor == null)
                return NotFound();

            return Ok(vendedor);
        }
        ///<summary>
        /// Método para inserir um novo vendedor.
        ///</summary>
        ///<param name="vendedor"></param>
        ///<returns>Retorna o vendedor inserido.</returns>
        [HttpPost("CriarVendedor")]
        public IActionResult CriarVendedor(Vendedor vendedor)
        {
            _context.Add(vendedor);
            _context.SaveChanges();

            return CreatedAtAction(nameof(ObterVendedorPorId), new { id = vendedor.VendedorId }, vendedor);
        }
        ///<summary>
        /// Método para atualizar um vendedor.
        ///</summary>
        ///<param name="id"></param>
        ///<param name="vendedor"></param>
        ///<returns>Retorna o vendedor atualizado, caso exista.</returns>
        [HttpPut("AtualizarVendedor")]
        public IActionResult AtualizarVendedor(int id, Vendedor vendedor)
        {
            var vendedorBanco = _context.Vendedores.Find(id);

            if(vendedorBanco == null)
                return NotFound();
            
            vendedorBanco.CPF       = vendedor.CPF;
            vendedorBanco.Nome      = vendedor.Nome;
            vendedorBanco.Email     = vendedor.Email;
            vendedorBanco.Telefone  = vendedor.Telefone;

            _context.Vendedores.Update(vendedorBanco);
            _context.SaveChanges();

            return Ok(vendedorBanco);
        }
        ///<summary>
        /// Método para remover um vendedor.
        ///</summary>
        ///<param name="id"></param>
        [HttpDelete("RemoverVendedor")]
        public IActionResult RemoverVendedor(int id)
        {
            var vendedorBanco = _context.Vendedores.Find(id);

            if(vendedorBanco == null)
                return NotFound();

            _context.Vendedores.Remove(vendedorBanco);
            _context.SaveChanges();

            return NoContent();
        }
    }

}