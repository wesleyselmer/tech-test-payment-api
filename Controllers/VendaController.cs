using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Entities;
using tech_test_payment_api.Models;


namespace tech_test_payment_api.Controllers
{
    ///<summary>
    /// Classe responsável pelas interações com a tabela Venda
    ///</summary>
    [ApiController]
    [Route("[controller]")]

    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;

        ///<summary>
        /// Definição da variável de interação context
        ///</summary>
        public VendaController(VendaContext context)
        {
            _context = context;
        }
        ///<summary>
        /// Método para obtenção de todas as vendas.
        ///</summary>
        ///<returns> Lista de vendas, com o vendedor responsável e os itens de cada venda</returns>
        [HttpGet("ObterTodasVendas")]
        public IActionResult ObterTodasVendas()
        {
            
            var vendas = _context.Vendas.ToList();

            foreach(Entities.Venda venda in vendas)
            {
                venda.Items = _context.Itens.Where(i => i.VendaId == venda.VendaId).ToList();
                venda.Vendedor = _context.Vendedores.Find(venda.VendedorId);
            }
            return Ok(vendas);
        }
    ///<summary>
    /// Classe responsável pela busca de vendas por Id da venda
    ///</summary>
    ///<param name="id"></param>
    ///<returns>Retorna a venda, com vendedor responsável e os itens relacionados.</returns>
        [HttpGet("{id}")]
        public IActionResult ObterPorId(int id)
        {
            var venda = _context.Vendas.Find(id);

            if(venda == null)
                return NotFound();

            venda.Items = _context.Itens.Where(i => i.VendaId == venda.VendaId).ToList();
            venda.Vendedor = _context.Vendedores.Find(venda.VendedorId);
        
            return Ok(venda);
        }
        ///<summary>
        /// Classe responsável pela busca de vendas por vendedor
        ///</summary>
        ///<param name="idVendedor"></param>
        ///<returns>Retorna as vendas encontradas, com vendedor responsável e os itens relacionados.</returns>
        [HttpGet("ObterPorVendedor")]
        public IActionResult ObterPorVendedor(int idVendedor)
        {
            var vendedorBanco = _context.Vendedores.Find(idVendedor);
            var vendas = _context.Vendas.Where(x => x.VendedorId == vendedorBanco.VendedorId).ToList();

            if(vendas == null)
                return NotFound();
            
            foreach(Entities.Venda venda in vendas)
            {
                venda.Items = _context.Itens.Where(i => i.VendaId == venda.VendaId).ToList();
                venda.Vendedor = _context.Vendedores.Find(venda.VendedorId);
            }

            return Ok(vendas);
        }
        ///<summary>
        /// Classe responsável pela busca de vendas por Status da venda
        ///</summary>
        ///<param name="status"></param>
        ///<returns>Retorna as vendas encontradas, com vendedor responsável e os itens relacionados.</returns>
        [HttpGet("ObterPorStatus")]
        public IActionResult ObterPorStatus(EnumStatusVenda status)
        {
            var vendas = _context.Vendas.Where(x => x.Status == status).ToList();
            
            if(vendas == null)
                return NotFound();

            foreach(Entities.Venda venda in vendas)
            {
                venda.Items = _context.Itens.Where(i => i.VendaId == venda.VendaId).ToList();
                venda.Vendedor = _context.Vendedores.Find(venda.VendedorId);
            }

            return Ok(status);
        }
        ///<summary>
        /// Classe responsável pela criação de uma nova venda
        ///</summary>
        ///<param name="venda"></param>
        ///<returns>Retorna a nova venda criada, com vendedor responsável e os itens relacionados.</returns>
        [HttpPost("CriarVenda")]
        public IActionResult CriarVenda([FromBody]Models.Venda venda)
        {
            var vendaBanco = new Entities.Venda();
            vendaBanco.VendedorId = venda.VendedorId;
            vendaBanco.Status = venda.Status;
            vendaBanco.Data = venda.Data;
            
            _context.Vendas.Add(vendaBanco);
            _context.SaveChanges();

            foreach(Item item in venda.Items)
            {
                item.VendaId = vendaBanco.VendaId;
                _context.Itens.Add(item);
            }

            _context.SaveChanges();
            return CreatedAtAction(nameof(ObterPorId), new { id = vendaBanco.VendaId }, vendaBanco);
        }
        ///<summary>
        /// Classe responsável por atualizar o Status da venda, seguindo as restrições de atualização.
        /// Status 0: Só pode ser alterado para 1 ou 4;
        /// Status 1: Só pode ser alterado para 2 ou 4;
        /// Status 2: Só pode ser alterado para 3.
        ///</summary>
        ///<param name="vendaId"></param>
        ///<param name="statusId"></param>
        ///<returns>Retorna a venda atualizada, com vendedor responsável e os itens relacionados.</returns>
        [HttpPut("AtualizarStatus")]
        public IActionResult AtualizarStatus(int vendaId, EnumStatusVenda statusId)
        {
            var venda = _context.Vendas.Find(vendaId);

            if(venda == null)
                return NotFound();

            switch (venda.Status)
            {
                case EnumStatusVenda.AguardandoPagamento:
                if (statusId != EnumStatusVenda.PagamentoAprovado && statusId != EnumStatusVenda.Cancelada)
                    return BadRequest($"Código de atualização de Status inválido. Código atual {venda.Status} só pode ser alterado para 1 ou 4");

                venda.Status = statusId;
                
                _context.Vendas.Update(venda);
                _context.SaveChanges();

                return Ok(venda);

                
                case EnumStatusVenda.PagamentoAprovado:
                if (statusId != EnumStatusVenda.EnviadoParaTransportadora && statusId != EnumStatusVenda.Cancelada)
                    return BadRequest($"Código de atualização de Status inválido. Código atual {venda.Status} só pode ser alterado para 2 ou 4");

                venda.Status = statusId;

                _context.Vendas.Update(venda);
                _context.SaveChanges();

                return Ok(venda);

                case EnumStatusVenda.EnviadoParaTransportadora:
                if (statusId != EnumStatusVenda.Entregue)
                    return BadRequest($"Código de atualização de Status inválido. Código atual {venda.Status} só pode ser alterado para 3");

                venda.Status = statusId;
                _context.Vendas.Update(venda);
                _context.SaveChanges();

                return Ok(venda);

                default:
                return NotFound();
            }

        }
        ///<summary>
        /// Classe responsável por excluir uma venda.
        ///</summary>
        ///<param name="vendaId"></param>
        [HttpDelete("ExcluirVenda")]
        public IActionResult ExcluirVenda(int vendaId)
        {
            var venda = _context.Vendas.Find(vendaId);

            if(venda == null)
                return NotFound();
            
            var itens = _context.Itens.Where(i => i.VendaId == venda.VendaId).ToList();

            foreach(Item item in itens)
            {
                _context.Itens.Remove(item);
                _context.SaveChanges();
            }

            _context.Vendas.Remove(venda);
            _context.SaveChanges();

            return NoContent();
        }
    }
}