﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace techtestpaymentapi.Migrations
{
    /// <inheritdoc />
    public partial class AjustesTabelas : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Itens_Vendas_VendaId",
                table: "Itens");

            migrationBuilder.AlterColumn<int>(
                name: "VendaId",
                table: "Itens",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Itens_Vendas_VendaId",
                table: "Itens",
                column: "VendaId",
                principalTable: "Vendas",
                principalColumn: "VendaId",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Itens_Vendas_VendaId",
                table: "Itens");

            migrationBuilder.AlterColumn<int>(
                name: "VendaId",
                table: "Itens",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Itens_Vendas_VendaId",
                table: "Itens",
                column: "VendaId",
                principalTable: "Vendas",
                principalColumn: "VendaId");
        }
    }
}
