using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using tech_test_payment_api.Entities;

namespace tech_test_payment_api.Models
{
    ///<summary>
    /// Classe modelo da Entity Venda
    ///</summary>
    public class Venda
    {
        ///<summary>
        /// Id do Vendedor, tipo int
        ///</summary>
        public int VendedorId           { get; set; }
        ///<summary>
        /// Status da venda, tipo EnumStatusVenda
        ///</summary>
        public EnumStatusVenda Status   { get; set; }
        ///<summary>
        /// Data da venda, tipo DateTime
        ///</summary>
        public DateTime Data            { get; set; }
        ///<summary>
        /// Itens da venda, tipo Lista de Item
        ///</summary>
        public List<Item> Items         { get; set; }
    }
}